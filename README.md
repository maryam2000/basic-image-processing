# Basic Image Processing
- This is my Signals and Systems Optional Course Project
- Implementation of Sobel and Kirsch Edge detection algorithms in MATLAB.
- Designed an algorithm that counts the number of circles in a given image using Hough Transform.
- Implementation of image segmentation algorithms like K-means and Otsu in MATLAB.
